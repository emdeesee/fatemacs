* FATE Accessories and Tools for Emacs

Turn Emacs into a Game Master's dashboard for Fate Core. Roll dice,
resolve actions, and generate NPCs as needed.

** Getting Started

*** Prerequisites

FATE Accessories and Tools for Emacs is currently self-contained, and
has no prequisites, except Emacs itself.

*** Installation

To install FATEmacs, use ~package-install-file~.

#+BEGIN_EXAMPLE
M-x package-install-file[RET]<pathname>/fatemacs.el[RET]
#+END_EXAMPLE

** Usage

There are several interactive commands available to help manage a Fate
session.

**** Display Fate Ladder

Use ~fatemacs-display-ladder~ to show the Fate Ladder with values and
descriptors in a buffer.

#+BEGIN_EXAMPLE
M-x fatemacs-display-ladder
#+END_EXAMPLE

**** Roll Fate/Fudge dice

Use ~fatemacs-roll-dF~ to roll 4dF and show the result in a buffer, which
accumulates the results of all the rolls, so you can look back and
previous rolls. You will be prompted to provide a skill level descriptor,
the value for which will be added to your roll. E.g.:

#+BEGIN_EXAMPLE
M-x fatemacs-roll-dF[RET]
Skill Level: Great[RET]
Fantastic (6)
#+END_EXAMPLE

In this case, =(+ □ + □)   6 Fantastic= is also printed to the
=*fatemacs-rolls*= buffer.

With a prefix argument, ~fatemacs-roll-dF~ will omit prompting for a
skill level, and simply return the 4dF result.

#+BEGIN_EXAMPLE
C-u M-x fatemacs-roll-dF[RET]
Terrible (-2)
#+END_EXAMPLE

Again, =(- - - +)  -2 Terrible= will be printed into the
=*fatemacs-rolls*= buffer.

**** Generate a random list of skills

~fatemacs-pick-skills~ will generate a random list of skills, the size
of which can be controlled by a prefix argument.

For example, the command ~C-u 10 M-x fatemacs-pick-skills[RET]~ will
result in 10 random skills being inserted into the results buffer.

**** Create a Random NPC

~make-fate-npc~ is a function that will product random NPCs, inserting
the ouput into the buffer =*NPC*=.

The NPC will have randomly selected attributes from name to skills and
stunts.

#+BEGIN_EXAMPLE
Beladhadwy
Adult Half-Elf Female

 High Concept:  Knight
      Trouble:  Sucker for a Pretty Face

                Look in My Eyes
                A Cut Above the Norm
                I've seen this before

Skills
    Great (+4): Resources
     Good (+3): Alchemy, Osteomancy
     Fair (+2): Investigate, Athletics, Burglary
  Average (+1): Will, Physique, Sail, Contacts

Stunts
Shipwright, Dazing Counter, Osteomantic Divination
#+END_EXAMPLE

** License

[[COPYING]]

** Acknowledgements

+ [[https://fate-srd.com/][Fate SRD]]
+ [[https://wiki.rpg.net/index.php/Aspects_List]]
