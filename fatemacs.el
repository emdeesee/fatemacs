;;; fatemacs.el --- FATE Accessories and Tools for Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2020  Michael Cornelius

;; Author: Michael Cornelius <michael@ninthorder.com>
;; URL: https://gitlab.com/kouneriasu/fatemacs
;; Keywords: games
;; Version: 0.0.0
;; Package-Requires: ((emacs "24.3"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Functions and information for using Emacs as a dashboard for
;; running a FATE Core-based RPG.

;;; Code:

(require 'cl-lib)

;;; Utilities

(defun fatemacs--shuffle (list)
  "Return a shuffled copy of LIST."
  (let ((shuffled (cl-copy-list list)))
    (loop for i from (1- (length list)) downto 1
          do (let ((j (random (1+ i))))
               (rotatef (nth i shuffled) (nth j shuffled)))
          finally (return shuffled))))


(defun fatemacs--random-sample (n list)
  "Choose N random elements from LIST without repetition, where N
is smaller than the length of the list."
  (assert (< n (length list)))
  (cl-subseq (fatemacs--shuffle list) 0 n))


(defun fatemacs--randomth (list)
  "Return a random element of LIST."
  (nth (random (length list)) list))


;; Weighted random choices...

(defun fatemacs--cumulative-weights (weights)
  "Return the total of `weights' and the cumulative distribution
table."
  (loop for w in weights
        sum w into total
        collect total into cumulative
        finally (return (values total cumulative))))


(defun fatemacs--bisect (list n)
  "Find the index where inserting `n' in the sorted list `list'
would maintain the sort."
  (loop for w in list
        for index = 0 then (1+ index)
        when (> w n) return index
        finally (return index)))


(defun fatemacs--weighted-choice (population weights)
  "Randomly choose among `population', where selection preference
is informed by `weights'. For example, with population (A B) and
weight (25 75), A should be chosen 25% of the time while B is
chosen 75%.

Weights need not be percentages; they can be any set of values.
Selections among the population will be made proportionally to
the sum of the weights."
  (when (listp population)
    (assert (= (length population) (length weights)))
    (multiple-value-bind (total cumulative) (fatemacs--cumulative-weights weights)
      (nth
       (fatemacs--bisect cumulative (random total))
       population))))


(defun fatemacs--read-file (pathname)
  "Read an elisp form from a file into a string."
  (read
   (with-temp-buffer
     (insert-file-contents pathname)
     (buffer-string))))


;;; Ladder

(defconst fatemacs-ladder-descriptors
  '("Terrible" "Poor" "Mediocre" "Average" "Fair" "Good" "Great" "Superb" "Fantastic" "Epic" "Legendary"))


(defconst fatemacs-ladder
  (loop
   for n from -2
   for d in fatemacs-ladder-descriptors
   collect (cons n d)))


(defconst fatemacs-ladder-min
  (cl-reduce #'min (mapcar #'car fatemacs-ladder)))


(defconst fatemacs-ladder-max
  (cl-reduce #'max (mapcar #'car fatemacs-ladder)))


(defun fatemacs-describe-value (value)
  "Describe VALUE, an integer, using FATE ladder descriptors."
  (cond
   ((< value fatemacs-ladder-min) (format "Worse than %s" (cdr (assoc fatemacs-ladder-min fatemacs-ladder))))
   ((> value fatemacs-ladder-max) (format "Better than %s" (cdr (assoc fatemacs-ladder-max fatemacs-ladder))))
   (t (cdr (assoc value fatemacs-ladder)))))


(defun fatemacs-evaluate-descriptor (descriptor)
  "Convert a string to a FATE ladder value.

DESCRIPTOR - the string to be converted.  If not a known
descriptor, NIL is returned."
  (car (rassoc descriptor fatemacs-ladder)))


(defconst fatemacs-ladder-buffer-name "*fatemacs-ladder*")


(defun fatemacs--make-ladder-buffer (name)
  (pop-to-buffer name)
  (loop for x in (reverse fatemacs-ladder)
        do (progn
             (insert (format "%6d %s" (car x) (cdr x)))
             (newline)))
  (read-only-mode 1))


(defun fatemacs-display-ladder (&optional buffer-name)
  (interactive)
  (let ((buffer-name (or buffer-name fatemacs-ladder-buffer-name)))
    (if (get-buffer buffer-name)
        (pop-to-buffer buffer-name)
        (fatemacs--make-ladder-buffer buffer-name))))


(defun fatemacs--read-skill-level ()
  "Read a descriptor from the minibuffer."
  (fatemacs-evaluate-descriptor
   (completing-read
    "Skill Level: "
    fatemacs-ladder-descriptors
    nil t nil
    'fatemacs--skill-selection-history
    fatemacs-ladder-descriptors)))


;;; Dice

;; Reseed the PRNG
(random t)

(defun fatemacs--roll-1-df ()
  "Generate a FATE die value, [-1,0,1]."
  (1- (random 3)))


(defun fatemacs-dF (&optional how-many)
  "Roll FATE dice; if HOW-MANY is NIL, four are rolled."
  (let ((n (or how-many 4)))
    (loop repeat n collect (fatemacs--roll-1-df))))


(defconst fatemacs--symbols '(- □ +)) ; TODO better symbols?


(defun fatemacs--number-to-symbol (number)
  "Convert from a NUMBER, given by `number', to a die face symbol."
  (assert (and (>= number -1) (<= number 1)))
  (nth (1+ number) fatemacs--symbols))


(defun fatemacs--dice-to-symbols (dice)
  "Convert DICE, a list of numbers, to symbols."
  (mapcar #'fatemacs--number-to-symbol dice))


;;;###autoload
(defun fatemacs-roll-dF (&optional exclude-skill)
  "Perform a 4dF roll and report the results.

If EXCLUDE-SKILL is non-NIL, the roll result will be based only
on the rest of the pseudo-random process used to generate FATE
dice values.  Otherwise, the user will be prompted to provide a
skill level."
  (interactive "P")
  (let* ((dice (fatemacs-dF))
         (total (cl-reduce #'+ dice
                        :initial-value (if exclude-skill 0
                                         (fatemacs--read-skill-level))))
         (desc (fatemacs-describe-value total)))

    (message "%s (%d)" desc total)

    (pop-to-buffer "*fatemacs-rolls*")
    (goto-char (point-max))
    (insert
     (format "%S %3d %s" (fatemacs--dice-to-symbols dice) total (fatemacs-describe-value total)))
    (newline)))


;;; Non-Player Character generation

;; Data sources

; TODO customizable
(defvar fatemacs-resource-directory
  (concat (file-name-directory (or load-file-name buffer-file-name))
          "resources/"))
(defvar fatemacs-name-directory (concat fatemacs-resource-directory "names/"))
(defvar fatemacs-aspect-directory (concat fatemacs-resource-directory "aspects/"))
(defvar fatemacs-race-directory (concat fatemacs-resource-directory "races/"))


;; race/culture

(defvar fatemacs-races (fatemacs--read-file (concat fatemacs-race-directory "races.sexp")))

(defun fatemacs--pick-race (&optional races)
  (fatemacs--randomth (or races fatemacs-races)))


;; gender

(defvar fatemacs-genders '(feminine masculine non-binary))
(defvar fatemacs-gender-frequencies '(45 45 10))

(defun fatemacs--pick-gender ()
  (fatemacs--weighted-choice fatemacs-genders fatemacs-gender-frequencies))

(defun fatemacs--gender-file-descriptor (gender)
  (symbol-name
   (if (eq gender 'non-binary)
       (fatemacs--randomth '(masculine feminine))
     gender)))

(defun fatemacs--gender-descriptor (gender)
  (case gender
    (masculine "male")
    (feminine "female")
    (t "non-binary")))


;; names

(defun fatemacs--get-name-files (&rest args)
  (directory-files fatemacs-name-directory nil ".*\\.sexp"))

(defun fatemacs--get-name-file-contents (file-name)
  (fatemacs--read-file (concat fatemacs-name-directory file-name)))

(defun fatemacs--get-name (&rest args)
  (fatemacs--randomth
   (cl-mapcan #'fatemacs--get-name-file-contents (apply #'fatemacs--get-name-files args))))



;; aspects

(defvar fatemacs-general-aspects '(allies disposition enemies expertise gear motto))

(defun fatemacs--aspect-file-name (aspect-class)
  (concat fatemacs-aspect-directory (symbol-name aspect-class) ".sexp"))

(defun fatemacs--pick-aspect-from-file (pathname)
  (fatemacs--randomth (fatemacs--read-file pathname)))

(defun fatemacs--pick-aspect (aspect-class)
  (fatemacs--pick-aspect-from-file (fatemacs--aspect-file-name aspect-class)))

(defun fatemacs--select-aspects (&optional n)
  "Pick N unique aspects from any general aspect class. If N is
not provided, three random aspects will be returned."
  (fatemacs--random-sample
   (or n 3)
   (cl-mapcan (lambda (aspect-class)
             (fatemacs--read-file
              (fatemacs--aspect-file-name aspect-class)))
           fatemacs-general-aspects)))


;; age

(defvar fatemacs-ages '(child adolescent adult middle-aged elderly))
(defvar fatemacs-age-frequencies '(5 15 35 30 15))

(defun fatemacs--pick-age ()
  (fatemacs--weighted-choice fatemacs-ages fatemacs-age-frequencies))


;; Skills

(defvar fatemacs-skills-file
  (concat fatemacs-resource-directory "skills/skills.sexp"))


(defun fatemacs-pick-skills (&optional n)
  "Pick N random skills from the available list, and put the result in a buffer for review.

By default, pick one skill, or, with a prefix argument, output N skills."
  (interactive "p")
  (pop-to-buffer "*fatemacs-skills*")
  (goto-char (point-max))
  (dolist (skill-group
           (fatemacs--random-sample
            (or n 1)
            (fatemacs--read-file fatemacs-skills-file)))
    (insert (format "%s\n" (car skill-group)))))


(defvar fatemacs-skill-filter-p (lambda (high-concept skill) t)
  "This value of this variable will be called by funcall when
  selecting skills; any skill for which nil is returned will be
  disallowed from selection. If a true value is returned, the
  skill will be available for selection. By default, all skills
  are allowed. Arguments to the function are a high
  concept (useful when considering a set of skills) and the skill
  itself.")


(cl-defun fatemacs--select-skills (&key (n 10) (allowed fatemacs-skill-filter-p) high-concept)
  "Select N skills, which are returned as a list.

Available skills are filtered by ALLOWED function, which is set
to the value of FATEMACS-SKILL-FILTER-P by default."
  (fatemacs--random-sample n
                           (remove-if-not (lambda (skill) (funcall allowed high-concept skill))
                                          (fatemacs--read-file fatemacs-skills-file))))


;; TODO Archetypes


;; Make an NPC! :D

;;;###autoload
(defun fatemacs-make-npc ()
  "Create a NPC; write the description into buffer *NPC*."
  (interactive)
  (pop-to-buffer "*NPC*")
  (goto-char (point-max))
  (let ((race (fatemacs--pick-race))
        (gender (fatemacs--pick-gender))
        (high-concept (fatemacs--pick-aspect 'high-concept)))
    (insert (capitalize (fatemacs--get-name race gender)))
    (newline)
    (insert (capitalize
             (format "%s %S %s\n\n"
                     (fatemacs--pick-age)
                     race
                     (fatemacs--gender-descriptor gender))))
    (insert (format " High Concept:  %s\n" high-concept))
    (insert (format "      Trouble:  %s\n"
                    (fatemacs--pick-aspect 'trouble)))
    (newline)
    (dolist (aspect (fatemacs--select-aspects))
      (insert (format "                %s\n" aspect)))
    (newline)
    (let* ((groups (fatemacs--select-skills :high-concept high-concept))
           (skills (mapcar #'car groups))
           (stunts (cl-mapcan #'cdr groups)))
      (insert "Skills\n")
      (insert "    Great (+4): ")
      (insert (format "%s\n"(car skills)))
      (insert "     Good (+3): ")
      (insert (format "%s\n" (mapconcat #'identity (cl-subseq skills 1 3) ", ")))
      (insert "     Fair (+2): ")
      (insert (format "%s\n" (mapconcat #'identity (cl-subseq skills 3 6) ", ")))
      (insert "  Average (+1): ")
      (insert (format "%s\n" (mapconcat #'identity (cl-subseq skills 6) ", ")))

      (newline)
      (insert "Stunts\n")
      (insert (format "%s\n" (mapconcat #'identity (fatemacs--random-sample 3 stunts) ", "))))

    (newline 2)))

(provide 'fatemacs)
;;; fatemacs.el ends here
