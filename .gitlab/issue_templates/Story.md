# Describe the requirement as a story

(Try thinking in these terms "When <some activity or event>, as a <type
of user>, <name> wants <some feature> because <some reason>, to achieve
<some goal>". The most important question is answer is "Why?")
